﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class ConsoleUI : MonoBehaviour
{
    [SerializeField] private TMP_Text m_resultsText = null;
    [SerializeField] private TMP_Text m_connectionText = null;

    [SerializeField] private TMP_InputField m_deviceIpIF = null;
    [SerializeField] private TMP_InputField m_portIF = null;

    private const string k_connectionStatusPattern = "<b>Status:</b> <color=#{0}>{1}</color> <b>- Last code:</b> <color=#{2}>{3}</color>";

    private const string k_statusConnectedColor = "00ab00";
    private const string k_statusConnectedStr = "Connected";
    private const string k_statusdisconnectedColor = "ab0000";
    private const string k_statusdisconnectedStr = "Disconnected";

    private const string k_lastCodeSuccessColor = "00ff00";
    private const string k_lastCodeErrorColor = "ff0000";


    public string InitConnectionStatusString()
    {
        string connectionStatusColor = ConsoleController.Instance.Connected ? k_statusConnectedColor : k_statusdisconnectedColor;
        string connectionStatusStr = ConsoleController.Instance.Connected ? k_statusConnectedStr : k_statusdisconnectedStr;

        return string.Format(k_connectionStatusPattern, connectionStatusColor, connectionStatusStr, ColorUtility.ToHtmlStringRGB(Color.white), "---");
    }

    private string GetConnectionSatusString(UnityWebRequest request)
    {
        if (!request.isDone)
            throw new System.Exception("Trying to get connections status string from not finished request.");

        string connectionStatusColor = ConsoleController.Instance.Connected ? k_statusConnectedColor : k_statusdisconnectedColor;
        string connectionStatusStr = ConsoleController.Instance.Connected ? k_statusConnectedStr : k_statusdisconnectedStr;
        string lastCodeColor = request.error == null ? k_lastCodeSuccessColor : k_lastCodeErrorColor;

        return string.Format(k_connectionStatusPattern, connectionStatusColor, connectionStatusStr, lastCodeColor, request.responseCode);
    }

    public void OnConnectButton()
    {
        ConnectToDevice();
    }

    public void OnDeviceIpSubmit(string input)
    {
        ConnectToDevice();
    }

    private async void ConnectToDevice()
    {
        if(!int.TryParse(m_portIF.text, out int port))
        {
            Debug.LogError($"Can't parse value ({m_portIF.text}) to int.");
            return;
        }

        m_connectionText.text = InitConnectionStatusString();
        UnityWebRequest response = await ConsoleController.ConnectToDevice(m_deviceIpIF.text, port);
        m_connectionText.text = GetConnectionSatusString(response);
    }

    private void Awake()
    {
        m_connectionText.text = InitConnectionStatusString();
        m_deviceIpIF.onSubmit.AddListener(OnDeviceIpSubmit);
        m_portIF.onSubmit.AddListener(OnDeviceIpSubmit);
        m_deviceIpIF.keyboardType = TouchScreenKeyboardType.NumbersAndPunctuation;
        m_portIF.keyboardType = TouchScreenKeyboardType.NumberPad;
    }
}
