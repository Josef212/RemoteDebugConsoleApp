﻿using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadTest : MonoBehaviour
{
    public string m_webUrl = "https://remotedebugconsoleweb.netlify.com/";

    [ContextMenu("Download")]
    public async void DownloadWeb()
    {

        UnityWebRequest request = new UnityWebRequest(m_webUrl);
        request.downloadHandler = new DownloadHandlerBuffer();

        Debug.Log("Sending request: " + request.ToString());

        request.SendWebRequest();

        while(!request.isDone)
        {
            Debug.Log(request.downloadProgress);
            await Task.Yield();
        }

        Debug.Log($"Finished. Error: {request.error }");
        Debug.Log($"Status: {request.responseCode}");

        Debug.Log($"Downloaded: {request.downloadHandler.text}");
    }

    [ContextMenu("Test")]
    public async void Test()
    {
        _ = await ConsoleController.ConnectToDevice("192.168.1.128", 55055);
    }

    [ContextMenu("Out")]
    public async void OutTest()
    {
        _ = await ConsoleController.OutRequest("192.168.1.128", 55055);
    }
}
