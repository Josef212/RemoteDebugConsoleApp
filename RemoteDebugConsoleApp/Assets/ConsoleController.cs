﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ConsoleController
{
    public static ConsoleController Instance => m_instance.Value;
    private static Lazy<ConsoleController> m_instance = new Lazy<ConsoleController>(() => new ConsoleController());

    public bool Connected { get; private set; } = false;
    public string ConnectedIP { get; private set; } = "";
    public int ConnectedPort { get; private set; } = 0;

    public static async Task<UnityWebRequest> ConnectToDevice(string ip, int port)
    {
        UnityWebRequest request = UnityWebRequest.Get(BuildUri(ip, port));
        request.downloadHandler = new DownloadHandlerBuffer();
        Debug.Log($"Sending to: {request.uri}");
        request.SendWebRequest();

        while (!request.isDone)
            await Task.Yield();

        Debug.Log($"Response code: {request.responseCode}");
        
        Instance.Connected = request.error == null;
        Instance.ConnectedIP = request.error == null ? ip : "";
        Instance.ConnectedPort = request.error == null ? port : 0;

        return request;
    }

    public static async Task<UnityWebRequest> OutRequest(string ip, int port)
    {
        UnityWebRequest request = UnityWebRequest.Get(BuildUri(ip, port, "console/out"));
        Debug.Log($"Sending to: {request.uri}");
        request.SendWebRequest();

        while (!request.isDone)
            await Task.Yield();

        Debug.Log($"Response code: {request.responseCode}");
        Instance.Connected = request.error == null;

        return request;
    }

    public static Uri BuildUri(string ip, int port, string path = "")
    {
        return new UriBuilder(ip)
        {
            Port = port,
            Scheme = "http",
            Path = path
        }.Uri;
    }
}
